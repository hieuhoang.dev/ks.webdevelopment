﻿using KS.BusinessLayer.IRepositories.Base;
using KS.DataLayer.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KS.BusinessLayer.IRepositories
{
    public interface IProductRepository : IGenericRepository<Product>
    {
        public int GetNumberOfProduct();
        Task<IEnumerable<Product>> GetProducts();
        Task<Product> GetProduct(Guid productId);
        Task<Product> AddProduct(Product product);
        Task<Product> UpdateProduct(Product product);
        Task<Product> DeleteProduct(Guid productId);
    }
}
