﻿using KS.BusinessLayer.IRepositories;
using KS.BusinessLayer.Repositories.Base;
using KS.DataLayer.Context;
using KS.DataLayer.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KS.BusinessLayer.Repositories
{
    public class ProductRepository : GenericRepository<Product>, IProductRepository
    {
        public ProductRepository(SalesManagementContext context) : base(context)
        {
        }

        public int GetNumberOfProduct()
        {
            return _context.Products.Count();
        }

        public async Task<IEnumerable<Product>> GetProducts()
        {
            return await _context.Products.ToListAsync();
        }

        public async Task<Product> GetProduct(Guid productId)
        {
            return await _context.Products
                .FirstOrDefaultAsync(e => e.ProductId == productId);
        }

        public async Task<Product> AddProduct(Product product)
        {
            var result = await _context.Products.AddAsync(product);
            await _context.SaveChangesAsync();
            return result.Entity;
        }

        public async Task<Product> UpdateProduct(Product product)
        {
            var result = await _context.Products
                .FirstOrDefaultAsync(e => e.ProductId == product.ProductId);

            if (result != null)
            {
                result.ProductName = product.ProductName;

                await _context.SaveChangesAsync();

                return result;
            }

            return null;
        }

        public async Task<Product> DeleteProduct(Guid productId)
        {
            var result = await _context.Products
                .FirstOrDefaultAsync(e => e.ProductId == productId);
            if (result != null)
            {
                _context.Products.Remove(result);
                await _context.SaveChangesAsync();
                return result;
            }
            return null;
        }
    }
}
