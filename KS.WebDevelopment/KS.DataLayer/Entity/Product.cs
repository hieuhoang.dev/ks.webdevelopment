﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KS.DataLayer.Entity
{
    public class Product
    {
        public Guid ProductId { get; set; }
        public string ProductName { get; set; }
        public bool IsAvailable { get; set; }
    }
}
