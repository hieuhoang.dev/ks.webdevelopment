﻿using KS.DataLayer.Config;
using KS.DataLayer.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KS.DataLayer.Context
{
    public class SalesManagementContext : DbContext
    {
        public SalesManagementContext(DbContextOptions options) : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ProductConfig());
        }

        public DbSet<Product> Products { get; set; }
    }
}
