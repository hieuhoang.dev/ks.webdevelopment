﻿// See https://aka.ms/new-console-template for more information
using KS.DemoDatabaseFirst.Entity;
using System.Text;

Console.OutputEncoding = Encoding.Unicode;

var context = new TrainingManagementContext();

var listTrainee = context.Trainees.ToList();
foreach (var traine in listTrainee)
{
    Console.WriteLine(traine.TraineeId + " | " + traine.TraineeName);
}