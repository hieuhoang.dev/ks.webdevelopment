﻿using System;
using System.Collections.Generic;

namespace KS.DemoDatabaseFirst.Entity;

public partial class Trainee
{
    public int TraineeId { get; set; }

    public string TraineeName { get; set; } = null!;

    public int Age { get; set; }

    public virtual ICollection<Subject> Subjects { get; set; } = new List<Subject>();
}
