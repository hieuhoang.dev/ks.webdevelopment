﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace KS.DemoDatabaseFirst.Entity;

public partial class TrainingManagementContext : DbContext
{
    public TrainingManagementContext()
    {
    }

    public TrainingManagementContext(DbContextOptions<TrainingManagementContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Subject> Subjects { get; set; }

    public virtual DbSet<Trainee> Trainees { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Server=DESKTOP-AGG82BP;Database=TrainingManagement;Trusted_Connection=True;TrustServerCertificate=True");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Subject>(entity =>
        {
            entity.HasKey(e => e.SubjectId).HasName("PK__Subject__AC1BA3A84E230A26");

            entity.ToTable("Subject");

            entity.Property(e => e.SubjectName).HasMaxLength(50);
        });

        modelBuilder.Entity<Trainee>(entity =>
        {
            entity.HasKey(e => e.TraineeId).HasName("PK__Trainee__3BA911CABF028B81");

            entity.ToTable("Trainee");

            entity.Property(e => e.TraineeName).HasMaxLength(50);

            entity.HasMany(d => d.Subjects).WithMany(p => p.Trainees)
                .UsingEntity<Dictionary<string, object>>(
                    "TraineeSubject",
                    r => r.HasOne<Subject>().WithMany()
                        .HasForeignKey("SubjectId")
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK__TraineeSu__Subje__29572725"),
                    l => l.HasOne<Trainee>().WithMany()
                        .HasForeignKey("TraineeId")
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK__TraineeSu__Train__286302EC"),
                    j =>
                    {
                        j.HasKey("TraineeId", "SubjectId").HasName("PK__TraineeS__A168ABF0CC2EBF80");
                        j.ToTable("TraineeSubject");
                    });
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
