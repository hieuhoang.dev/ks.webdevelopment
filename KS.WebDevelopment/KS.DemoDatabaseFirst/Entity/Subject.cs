﻿using System;
using System.Collections.Generic;

namespace KS.DemoDatabaseFirst.Entity;

public partial class Subject
{
    public int SubjectId { get; set; }

    public string SubjectName { get; set; } = null!;

    public virtual ICollection<Trainee> Trainees { get; set; } = new List<Trainee>();
}
